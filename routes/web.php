<?php

use App\Http\Controllers\Backend\AuthController;
use App\Http\Controllers\Backend\BookingController;
use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\TicketController;
use App\Http\Controllers\Backend\UserController;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('app.frontend.index');
});

Route::get('login', [AuthController::class, 'loginForm'])->name('login-form');
Route::get('register', [AuthController::class, 'registerForm'])->name('register-form');
Route::post('register', [AuthController::class, 'register'])->name('register');
Route::post('login', [AuthController::class, 'login'])->name('login');

Route::prefix('application')->middleware('auth')->group(function() {
    Route::get('register-google2fa', [AuthController::class, 'registerGoogle2Fa'])->name('register-google2fa');
    Route::get('complete-registration', [AuthController::class, 'completeRegistration'])->name('complete-registration');

    // Route::middleware('2fa  ')->group(function() {
        Route::get('logout', [AuthController::class, 'logout'])->name('logout');
        Route::post('/2fa', function () {
            return redirect()->route('booking.index');
        })->name('2fa');
        Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
        
        Route::prefix('user')->name('user.')->group(function() {
            Route::get('/', [UserController::class, 'index'])->name('index');
            Route::get('create', [UserController::class, 'create'])->name('create');
            Route::post('store', [UserController::class, 'store'])->name('store');
            Route::get('show/{id}', [UserController::class, 'show'])->name('show');
            Route::post('update/{id}', [UserController::class, 'update'])->name('update');
            Route::delete('delete/{id}', [UserController::class, 'delete'])->name('delete');
        });

        Route::prefix('booking')->name('booking.')->group(function() {
            Route::get('/', [BookingController::class, 'index'])->name('index');
            Route::get('create', [BookingController::class, 'create'])->name('create');
            Route::post('store', [BookingController::class, 'store'])->name('store');
            Route::get('show/{id}', [BookingController::class, 'show'])->name('show');
            // Route::post('update/{id}', [BookingController::class, 'update'])->name('update');
            Route::delete('delete/{id}', [BookingController::class, 'delete'])->name('delete');
            Route::post('upload/proof-of-payment', [BookingController::class, 'uploadProofOfPayment'])->name('upload-proof-of-payment');
            Route::post('approve-payment', [BookingController::class, 'approvePayment'])->name('approve-payment');
        });

        Route::prefix('ticket')->name('ticket.')->group(function() {
            Route::get('/', [TicketController::class, 'index'])->name('index');
            Route::get('create', [TicketController::class, 'create'])->name('create');
            Route::post('store', [TicketController::class, 'store'])->name('store');
            Route::get('show/{id}', [TicketController::class, 'show'])->name('show');
            Route::post('update/{id}', [TicketController::class, 'update'])->name('update');
            Route::delete('delete/{id}', [TicketController::class, 'delete'])->name('delete');
        });

        Route::get('qr-scanner', function() {
            return view('app.backend.qr-code.qr-scanner', ['activePage' => 'QR Scanner']);
        })->name('qr-scanner');
    // });
});

Route::get('checking-qr', [BookingController::class, 'checkingQr']);