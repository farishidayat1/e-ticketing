<?php

namespace App\Console\Commands;

use App\Models\Booking;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Crypt;
use Spatie\Crypto\Rsa\PrivateKey;
use Spatie\Crypto\Rsa\PublicKey;
use Str;

class Testing extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'test';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $barcode = Booking::find(1)->barcode;
        // $barcode = Crypt::decrypt($barcode);
        // $publicKey = PublicKey::fromFile(resource_path('key2/publickey'));

        // $barcode = $publicKey->decrypt($barcode);
        // dd($barcode);

        $data = 'my secret data';
        dd(Crypt::encrypt($data));
        $privateKey = PrivateKey::fromFile(resource_path('key2/privatekey'));
        $encryptedData = $privateKey->encrypt($data); 
        // $encryptedData = base64_encode($encryptedData);
        $encryptedData = urlencode(base64_encode($encryptedData));
        $decryptedData = urldecode(base64_decode($encryptedData));
        dd($encryptedData, $decryptedData);
        $publicKey = PublicKey::fromFile(resource_path('key2/publickey'));
        $decryptedData = $publicKey->decrypt($encryptedData); 
        // Booking::first()->update([
        //     'barcode' => Crypt::encrypt($encryptedData)
        // ]);

        dd(strlen($encryptedData), $decryptedData);

        // openssl_public_encrypt($message, $encrypted, file_get_contents(resource_path('key/publickey.pem')));
        // openssl_private_decrypt($encrypted, $decrypted, file_get_contents(resource_path('key/privatekey.pem')));

        // $this->info($encrypted);
        // $this->info($decrypted);


        // $plaintext = EasyRSA::decrypt($ciphertext, config);

        // $barcode = random_int(0, 999999999999).Carbon::now()->format('Ymd');
        // $encrypt = Crypt::encrypt($barcode);
        // $decrypt = Crypt::decrypt($encrypt);
        
        // $this->info($barcode);
        // $this->info($encrypt);
        
        // $this->info($decrypt);

    }
}
