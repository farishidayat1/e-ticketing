<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingDetail extends Model
{
    use HasFactory;

    protected $table = 'booking_details';

    protected $fillable = [
        'booking_id',
        'ticket_id',
        'qty',
        'amount'
    ];

    public function ticket()
    {
        return $this->belongsTo(Ticket::class);
    }
}
