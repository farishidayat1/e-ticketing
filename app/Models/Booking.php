<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Booking extends Model
{
    use HasFactory;

    protected $table = 'bookings';

    protected $fillable = [
        'booking_code',
        'user_id',
        'date',
        'status',
        'amount',
        'proof_of_payment',
        'barcode',
        'is_used'
    ];

    public function bookingDetails()
    {
        return $this->hasMany(BookingDetail::class, 'booking_id', 'id');
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
