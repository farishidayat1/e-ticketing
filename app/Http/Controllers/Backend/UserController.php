<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class UserController extends Controller
{
    public function index()
    {
        $users = User::where('id', '!=', auth()->user()->id)->orderBy('created_at', 'DESC')->get();

        return view('app.backend.user.index',[
            'activePage' => 'Users',
            'users' => $users
        ]);
    }

    public function create() 
    {
        return view('app.backend.user.create', [
            'activePage' => 'Create User'
        ]);
    }

    public function store(Request $request) 
    {
      $this->validate($request, [
        'name' => 'required',
        'username' => 'required',
        'email' => 'required|email',
        'phone' => 'required',
        'password' => 'required',
        'role' => 'required',
        'gender' => 'required',
        'email' => 'required',
        'address' => 'required',
      ]);

      $checkEmail = User::where('email', $request->email)->first();

      if(! empty($checkEmail)) {
        return redirect()->back()->withErrors(['error' => 'Email already Registered!']);
      }

      $user = User::create([
        'name' => $request->name,
        'username' => $request->username,
        'email' => $request->email,
        'phone' => $request->phone,
        'password' => Hash::make($request->password),
        'user_level' => $request->role,
        'gender' => $request->gender,
        'email' => $request->email,
        'address' => $request->address,
      ]);

      return redirect()->route('user.index')->with('success', 'Successfully Created User');
    }

    public function show($id) 
    {
        $user = User::find($id);

        return view('app.backend.user.edit', [
            'activePage' => 'edit',
            'user' => $user
        ]);
    }

    public function update(Request $request, $id) 
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'role' => 'required',
            'gender' => 'required',
            'email' => 'required',
            'address' => 'required',
          ]);

        $user = User::find($id)->update([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'user_level' => $request->role,
            'gender' => $request->gender,
            'email' => $request->email,
            'address' => $request->address,
        ]);
       
        if(! empty($request->password)) {
            $user->update([
                'password' => Hash::make($request->password)
            ]);
        }

        return redirect()->route('user.index')->with('success', 'Successfully Updated User');
    }

    public function delete($id) 
    {
        User::find($id)->delete();

        return redirect()->back()->with('success', 'Success Deleted User');
    }
}
