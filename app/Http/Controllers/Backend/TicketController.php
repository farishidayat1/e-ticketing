<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Ticket;
use Illuminate\Http\Request;

class TicketController extends Controller
{
    public function index()
    {
        $tickets = Ticket::latest()->get();

        return view('app.backend.ticket.index',[
            'activePage' => 'Ticket',
            'tickets' => $tickets
        ]);
    }

    public function create() 
    {
        return view('app.backend.ticket.create', [
            'activePage' => 'Ticket'
        ]);
    }

    public function store(Request $request) 
    {
        $this->validate($request, [
            'name' => 'required',
            'qty' => 'required|min:1',
            'price' => 'required|min:1',
        ]);

        $ticket = Ticket::create([
            'name' => $request->name,
            'qty' => $request->qty,
            'price' => $request->price,
        ]);

        return redirect()->route('ticket.index')->with('success', 'Successfully Created Ticket');
    }

    public function show($id) 
    {
        $ticket = Ticket::find($id);

        return view('app.backend.ticket.edit', [
            'activePage' => 'Ticket',
            'ticket' => $ticket
        ]);
    }

    public function update(Request $request, $id) 
    {
        $this->validate($request, [
            'name' => 'required',
            'qty' => 'required|min:1',
            'price' => 'required|min:1',
        ]);

        $ticket = Ticket::find($id)->update([
            'name' => $request->name,
            'qty' => $request->qty,
            'price' => $request->price,
        ]);

        return redirect()->route('ticket.index')->with('success', 'Successfully Updated Ticket');
    }

    public function delete($id) 
    {
        Ticket::find($id)->delete();

        return redirect()->back()->with('success', 'Success Deleted Ticket');
    }
}
