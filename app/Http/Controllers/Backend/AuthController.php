<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    public function loginForm()
    {
        if(Auth::check()) {
            if(!Auth::user()->google2fa_secret) {
                return redirect()->route('register-google2fa');
            }
            return redirect()->route('dashboard');
        } else {
            return view('app.backend.auth.login');
        }
    }

    public function registerForm()
    {
        return view('app.backend.auth.register');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'username' => 'required', 
            'password' => 'required'
        ]);

        $user = User::where('username', $request->username)->first();
        if(empty($user)) {
            return redirect()->back()->withErrors(['error' => 'User is not found!']);
        }

        if(!Hash::check($request->password, $user->password)) {
            return redirect()->back()->withErrors(['error' => 'Password is not match!']);
        }

        Auth::attempt([
            'username' => $request->username,
            'password' => $request->password
        ]);

        if(empty(auth()->user()->google2fa_secret)) {
            return redirect()->route('register-google2fa');
        } 
        
        if(auth()->user()->user_level == 'Admin') {
            return redirect()->route('dashboard');
        } else {
            return redirect()->route('booking.index');
        }
    }

    public function register(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'username' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed',
            'gender' => 'required',
            'address' => 'required'
        ]);

        User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
            'gender' => $request->gender,
            'address' => $request->address,
            'user_level' => 'User'
        ]);

        Session::flash('message', 'Successfully registred!'); 
        return redirect()->route('login-form');
    }

    public function logout(Request $request)
    {
        Auth::logout();
    
        $request->session()->invalidate();
    
        $request->session()->regenerateToken();
    
        return redirect('/login');
    }

    public function registerGoogle2Fa(Request $request) 
    {
        // Initialise the 2FA class
        $google2fa = app('pragmarx.google2fa');

        // Add the secret key to the registration data
        $google2fa_secret = $google2fa->generateSecretKey();

        // Generate the QR image. This is the image the user will scan with their app
        // to set up two factor authentication
        $QR_Image = $google2fa->getQRCodeInline(
            config('app.name'),
            auth()->user()->email,
            $google2fa_secret
        );

        User::where('id', auth()->user()->id)->update([
            'google2fa_secret' => $google2fa_secret
        ]);

        // Pass the QR barcode image to our view
        return view('app.backend.google2fa.register', [
            'QR_Image' => $QR_Image, 
            'secret' => $google2fa_secret,
            'activePage' => ''
        ]);
    } 

    public function completeRegistration(Request $request)
    {
        return view('app.backend.google2fa.index');
    }
}
