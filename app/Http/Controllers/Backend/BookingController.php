<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Barcode;
use App\Models\Booking;
use App\Models\BookingDetail;
use App\Models\Ticket;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Storage;

class BookingController extends Controller
{
    public function index(Request $request)
    {
        if($request->success_message) {
            Session::flash('success', 'Success created booking'); 
        }

        $bookings = Booking::latest();

        if(auth()->user()->user_level == 'User') {
            $bookings = $bookings->where('user_id', auth()->user()->id);
        }
        
        $bookings = $bookings->get();

        return view('app.backend.booking.index',[
            'activePage' => 'Booking',
            'bookings' => $bookings
        ]);
    }

    public function create() 
    {
        $tickets = Ticket::latest()->get();

        return view('app.backend.booking.create', [
            'activePage' => 'Booking',
            'tickets' => $tickets
        ]);
    }

    public function store(Request $request) 
    {
        if(empty($request->date)) {
            return response()->json([
                'error' => true,
                'message' => 'Please select a date!'
            ], 410);
        }
        
        if(empty($request->ticket_selected)) {
            return response()->json([
                'error' => true,
                'message' => 'Please select the ticket!'
            ], 410);
        }

        $booking = Booking::create([
            'booking_code' => 'BK-'.random_int(0, 999999).Carbon::now()->format('YmdHi'),
            'user_id' => Auth::user()->id,
            'date' => $request->date,
            'status' => 'Ordered',
            'amount' => $request->total_amount
        ]);

        foreach($request->ticket_selected as $ticket) {
            $bookingDetail = BookingDetail::create([
                'booking_id' => $booking->id,
                'ticket_id' => $ticket['id'],
                'qty' => $ticket['qty'],
                'amount' => $ticket['amount']
            ]);
        }

        return response()->json([
            'error' => false,
            'message' => 'Success created booking'
        ]);
    }

    public function show($id) 
    {
        $booking = Booking::find($id);

        $barcode = ! empty($booking->barcode) ? $booking->barcode : null;
        // openssl_public_encrypt($barcode, $encrypted, file_get_contents(resource_path('key/publickey.pem')));

        $encrypt = Crypt::encrypt($barcode);
        $content = 'http://localhost:8000/checking-qr?barcode='.$encrypt;

        $generateQr = "https://image-charts.com/chart?chs=150x150&cht=qr&choe=UTF-8&chl=".$content;

        return view('app.backend.booking.show', [
            'activePage' => 'Booking',
            'booking' => $booking,
            'generateQr' => $generateQr
        ]);
    }

    public function update(Request $request, $id) 
    {
        $this->validate($request, [
            'date' => 'required',
            'amount' => 'required|min:1',
        ]);

        $items = json_decode($request->items);
        if(count($items) == 0) {
            return redirect()->back()->withErrors(['error' => 'Please select the ticket!']);
        }

        $booking = Booking::find($id)->update([
            'booking_code' => 'BK-'.random_int(0, 999999).Carbon::now()->format('YmdHi'),
            'user_id' => Auth::user()->id,
            'date' => $request->date,
            'status' => 'Ordered',
            'amount' => $request->amount
        ]);
        
        BookingDetail::where('booking_id', $id)->delete();

        foreach($items as $item) {
            $bookingDetail = BookingDetail::create([
                'booking_id' => $booking->id,
                'ticket_id' => $item->ticket_id,
                'qty' => $item->qty,
                'amount' => $item->amount
            ]);
        }

        return redirect()->route('booking.index')->with('success', 'Successfully Updated Booking');
    }

    public function delete($id) 
    {
        Booking::find($id)->delete();
        BookingDetail::where('booking_id', $id)->delete();

        return redirect()->back()->with('success', 'Success Deleted Booking');
    }

    public function uploadProofOfPayment(Request $request)
    {
        if($request->file('proof_of_payment')) {
            $imageUrl = $request->file('proof_of_payment')->store('proofOfPayment', ['disk' => 'my_files']);
            Booking::find($request->booking_id)->update([
                'proof_of_payment' => $imageUrl,
                'status' => 'Waiting Approval'
            ]);

            return redirect()->back()->with('success', 'Success Uploaded Proof Of Payment');
        } else {
            return redirect()->back()->withErrors(['error' => 'Error Uploaded Proof Of Payment']);
        }
    }

    public function approvePayment(Request $request) 
    {
        if($request->approved) {
            Booking::find($request->booking_id)->update([
                'status' => 'Approved',
                'barcode' => str_shuffle(Carbon::now()->format('Ymd').random_int(0, 99999999999999999))
            ]);
            return redirect()->back()->with('success', 'Success Approve Payment');
        } else {
            return redirect()->back()->withErrors(['error' => 'Error Approve Payment']);
        }
    }

    public function checkingQr(Request $request)
    {
        $barcode = $request->barcode;
        $barcode = Crypt::decrypt($barcode);

        $booking = Booking::where('barcode', $barcode)->first();
        if(!$booking) {
            return 'Booking not found!';
        }

        if(Carbon::createFromFormat('Y-m-d', $booking->date)->format('Y-m-d') != Carbon::now()->format('Y-m-d')) {
            return 'QR Expired!';
        }

        if($booking->is_used) {
            return 'QR Already used!';
        }

        $booking->update(['is_used' => 1]);

        return view('app.backend.booking.show-by-qr', [
            'activePage' => 'Booking',
            'booking' => $booking
        ]);
    }
}
