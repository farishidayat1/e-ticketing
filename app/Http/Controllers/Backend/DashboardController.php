<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Booking;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index(Request $request)
    {
        if($request->q != null) {
            if($request->q == 'Daily') {
                $startDateCarbon = Carbon::now()->startOfDay()->format('Y-m-d H:i:s');
                $endDateCarbon = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');
            } else if ($request->q == 'Weekly') {
                $startDateCarbon = Carbon::now()->subWeek(1)->startOfDay()->format('Y-m-d H:i:s');
                $endDateCarbon = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');
            } else if ($request->q == 'Monthly') {
                $startDateCarbon = Carbon::now()->startOfDay()->subMonth(1)->format('Y-m-d H:i:s');
                $endDateCarbon = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');
            } else if ($request->q == 'Yearly') {
                $startDateCarbon = Carbon::now()->startOfDay()->subYear(1)->format('Y-m-d H:i:s');
                $endDateCarbon = Carbon::now()->endOfDay()->format('Y-m-d H:i:s');
            }

            $totalVisitors = Booking::whereBetween('created_at', [$startDateCarbon, $endDateCarbon])->where('is_used', 1)->count();
            $totalAmount = Booking::whereBetween('created_at', [$startDateCarbon, $endDateCarbon])->where('status', 'Approved')->sum('amount');
            $totalUser = User::whereBetween('created_at', [$startDateCarbon, $endDateCarbon])->count();
            $totalBooking = Booking::whereBetween('created_at', [$startDateCarbon, $endDateCarbon])->count();
        }

        return view('app.backend.dashboard', [
            'activePage' => 'Dashboard',
            'q' => $request->q,
            'totalVisitors' => $totalVisitors ?? 0,
            'totalAmount' => $totalAmount ?? 0,
            'totalUser' => $totalUser ?? 0,
            'totalBooking' => $totalBooking ?? 0
        ]);
    }
}
