@extends('app.backend.auth.layout.app')

@section('contents')
    <div class="panel panel-default">
        <div class="panel-heading font-weight-bold">Register</div>
        <hr>
        @if($errors->any())
            <b style="color: red">{{$errors->first()}}</b>
        @endif

        <div class="panel-body">
            <form class="form-horizontal" method="POST" action="{{ route('2fa') }}">
                {{ csrf_field() }}

                <div class="form-group">
                    <p>Please enter the  <strong>OTP</strong> generated on your Authenticator App. <br> Ensure you submit the current one because it refreshes every 30 seconds.</p>
                    <label for="one_time_password" class="col-md-4 control-label">One Time Password</label>


                    <div class="col-md-12">
                        <input id="one_time_password" type="number" class="form-control" name="one_time_password" required autofocus>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-md-12 col-md-offset-4">
                        <button type="submit" class="btn btn-primary">
                            Login
                        </button>
                        {{-- <p class="text-sm mt-3 mb-0"><a href="{{ route('register-google2fa') }}" class="text-dark font-weight-bolder">Scan QR Again</a></p> --}}
                    </div>
                </div>
            </form>
        </div>
    </div>
@endsection