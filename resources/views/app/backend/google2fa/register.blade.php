@extends('app.backend.auth.layout.app')

@section('contents')
<div class="panel panel-default">
    <div class="panel-heading font-weight-bold">Set up Google Authenticator</div>
    <hr>

    <div class="panel-body" style="text-align: center;">
        <p>Set up your two factor authentication by scanning the barcode below with you Google Authenticator app. Alternatively, you can use the code <strong> {{ $secret }}</strong> </p>
        <div>
            <img src="{{ $QR_Image }}">
        </div>
        
        <p>Take note of the <strong>OTP</strong> generated on your Authenticator App.You will be unable to login otherwise </p>
        <div>
            <a href="{{ route('complete-registration') }}"><button class="btn btn-primary">Finish Registration</button></a>
        </div>
    </div>
</div>
@endsection