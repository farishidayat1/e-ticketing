@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
    <div class="container-fluid py-4">
        <div class="row">
            <div class="col-12">
              <div class="card mb-4">
                <div class="card-header pb-0">
                  <h6>QR Scanner</h6>
                </div>
                <div class="card-body px-0 pt-0 pb-2">
                    <div style="width: 500px" id="reader"></div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection

@section('scripts')
<script src="{{ asset('html5-qrcode.min.js') }}"></script>

<script type="text/javascript">
    function onScanSuccess(decodedText, decodedResult) {
        // Handle on success condition with the decoded text or result.
        console.log(`Scan result: ${decodedText}`, decodedResult);
        window.open(decodedText, 'blank');
        html5QrcodeScanner.clear();
    }

    function onScanError(errorMessage) {
        // handle on error condition, with error message
        console.log("Error", errorMessage)
    }

    var html5QrcodeScanner = new Html5QrcodeScanner(
        "reader", { fps: 10, qrbox: 250 });
    html5QrcodeScanner.render(onScanSuccess);
</script>
@endsection