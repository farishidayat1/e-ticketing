@extends('app.backend.auth.layout.app')

@section('contents')
@include('app.backend.auth.layout.partials.alert-message')
<form id="form-register" action="{{ route('register') }}" method="POST">
    @csrf
    <div class="mb-3">
      <input type="text" class="form-control" placeholder="Name" aria-label="Name" name="name">
    </div>
    <div class="mb-3">
      <input type="email" class="form-control" placeholder="Email" aria-label="Email" name="email">
    </div>
    <div class="mb-3">
        <input type="text" class="form-control" placeholder="Username" aria-label="Username" name="username">
    </div>
    <div class="mb-3">
        <input type="text" class="form-control" placeholder="Phone" aria-label="Phone" name="phone">
    </div>
    <div class="mb-3">
      <input type="password" class="form-control" placeholder="Password" aria-label="Password" name="password">
    </div>
    <div class="mb-3">
        <input type="password" class="form-control" placeholder="Password Confirmation" aria-label="Password Confirmation" name="password_confirmation">
    </div>
    <div class="mb-3">
        <input type="text" class="form-control" placeholder="Gender" aria-label="Gender" name="gender">
    </div>
    <div class="mb-3">
        <input type="text" class="form-control" placeholder="Address" aria-label="Address" name="address">
    </div>
    <div class="form-check form-check-info text-start">
      <input class="form-check-input" type="checkbox" value="1" id="flexCheckDefault">
      <label class="form-check-label" for="flexCheckDefault">
        I agree the <a href="javascript:;" class="text-dark font-weight-bolder">Terms and Conditions</a>
      </label>
    </div>
    <div class="text-center">
      <button disabled id="sign-up-btn" type="button" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign up</button>
    </div>
    <p class="text-sm mt-3 mb-0">Already have an account? <a href="{{ route('login-form') }}" class="text-dark font-weight-bolder">Sign in</a></p>
</form>   
@endsection

@section('scripts')
    <script type="text/javascript">
        $('#flexCheckDefault').on('click', function() {
            const value = $(this).is(":checked");
            if(!value) {
                $('#sign-up-btn').prop('disabled', true)
            } else {
                $('#sign-up-btn').prop('disabled', false)
                $('#form-register').submit();
            }
        });
    </script>
@endsection