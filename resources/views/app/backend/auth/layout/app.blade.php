<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('assets/backend/img/apple-icon.png') }}">
  <link rel="icon" type="image/png" href="{{ asset('assets/backend/img/favicon.png') }}">
  <title>
    E Ticket
  </title>
  @include('app.backend.auth.layout.partials.styles')
</head>

<body class="">
  {{-- @include('app.backend.auth.layout.partials.navbar') --}}
  <main class="main-content  mt-0">
    <div class="page-header align-items-start min-vh-50 pt-5 pb-11 m-3 border-radius-lg" style="background-image: url('https://raw.githubusercontent.com/creativetimofficial/public-assets/master/argon-dashboard-pro/assets/img/signup-cover.jpg'); background-position: top;">
      <span class="mask bg-gradient-dark opacity-6"></span>
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-lg-5 text-center mx-auto">
            <h1 class="text-white mb-2 mt-5">Welcome!</h1>
            <p class="text-lead text-white">
              Please fill in your data in the form below.</p>
          </div>
        </div>
      </div>
    </div>
    <div class="container">
      <div class="row mt-lg-n10 mt-md-n11 mt-n10 justify-content-center">
        <div class="col-xl-4 col-lg-5 col-md-7 mx-auto">
          <div class="card z-index-0">
            <div class="card-body">
              @yield('contents')
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
  {{-- @include('app.backend.auth.layout.partials.footer') --}}
  @include('app.backend.auth.layout.partials.scripts')
</body>

</html>