@extends('app.backend.auth.layout.app')

@section('contents')
@include('app.backend.auth.layout.partials.alert-message')
<form action="{{ route('login') }}" method="POST">
    @csrf
    <div class="mb-3">
      <input type="text" class="form-control" placeholder="Username" aria-label="Name" name="username">
    </div>
    <div class="mb-3">
      <input type="password" class="form-control" placeholder="Password" aria-label="Password" name="password">
    </div>
    <div class="text-center">
      <button type="submit" class="btn bg-gradient-dark w-100 my-4 mb-2">Sign in</button>
    </div>
    <p class="text-sm mt-3 mb-0">You don't have account? <a href="{{ route('register-form') }}" class="text-dark font-weight-bolder">Sign up</a></p>
</form>   
@endsection