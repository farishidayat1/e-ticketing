@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              @if(session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
              @if(auth()->user()->user_level == 'User')
                <a href="{{ route('user.create') }}" class="btn btn-primary">Create User</a>
              @endif
              <h6>Users table</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center justify-content-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Email</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Phone</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">User Level</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Gender</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Address</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($users as $user)
                      <tr>
                        <td>
                          <h6 class="mb-0 text-sm" style="margin-left: 10px;">{{ $user->name }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->email }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->phone }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->user_level }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->gender }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $user->address }}</h6>
                        </td>
                        <td class="align-middle d-flex flex-wrap">
                          <a style="padding:4px 0;" class="btn btn-link text-secondary mb-0" href="{{ route('user.show', $user->id) }}">
                            <i class="fas fa-eye"></i>
                          </a>
                          @if(auth()->user()->id != $user->id)
                            <form  method="post" action="{{ route('user.delete', $user->id) }}">
                              @csrf
                              @method('DELETE')
                              <button style="padding:0" type="submit" class="btn btn-link text-secondary mb-0 mx-2">
                                <i class="fas fa-trash"></i>
                              </button>
                            </form>
                          @endif
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
@endsection