@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
   <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<i class="fas fa-plus"></i>
						Create Booking
					</div>
					<div class="card-body">
						@include('app.backend.auth.layout.partials.alert-message')
						<div>
							<div class="form-group">
								<label>Date</label>
								<input type="date" name="date" class="form-control @error('date') is-invalid @enderror">
								@error('date')
								 	<div id="validationServerDateFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Ticket</label>
								<div>
									<div class="row">
										@foreach($tickets as $ticket)
											<input type="hidden" name="ticketId[]" value="{{ $ticket->id }}">
											<div class="col-sm-5">
												<div class="card">
													<div class="card-body">
														<h5 class="card-title">{{ $ticket->name }}</h5>
														<p class="card-text">Rp. {{ number_format($ticket->price) }}</p>
														<div class="input-group">
															<span class="input-group-text">Quantity :</span>
															<input type="number" class="form-control ticket-qty" name="ticketQty[]" min="0" placeholder="0" data-id="{{ $ticket->id }}" data-name="{{ $ticket->name }}" data-price="{{ $ticket->price }}">
														</div>
													</div>
												</div>
											</div>
										@endforeach
									  </div>
								</div>
							</div>
							<div class="form-group">
								<label>Booking Summary</label>
								<ul class="list-group">
									<li class="list-group-item active">List Ticket Selected</li>
								</ul>
							</div>
							<button class="btn btn-primary btn-submit">Create Booking</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>
@endsection

@section('scripts')
<script type="text/javascript">
	var ticketSelected = [];
	var totalAmount = 0;

	$(".ticket-qty").change(function () {
		let ticketQty = $(this).val() 
		let ticketId = $(this).attr('data-id')
		let ticketName = $(this).attr('data-name')
		let ticketPrice = $(this).attr('data-price')
		let totalAmountTicket = 0;

		ticketSelected = ticketSelected.filter(item => item.id !== ticketId)

		if(ticketQty != 0) {
			ticketSelected.push({
				'id': ticketId,
				'name': ticketName,
				'qty': ticketQty,
				'amount': (parseInt(ticketQty) * parseFloat(ticketPrice))
			})
		}

		$(".list-group").empty()
		$(".list-group").append(`<li class="list-group-item active">List Ticket Selected</li>`)
		ticketSelected.forEach(ticket => {
			totalAmountTicket += ticket.amount;
			const formattedPrice = ticket.amount.toLocaleString('id-ID')
			$(".list-group").append(`<li class="list-group-item">Ticket: ${ticket.name}, Qty: ${ticket.qty}, Amount: Rp. ${formattedPrice} </li>`)
		});
		if(ticketSelected.length > 0) {
			const formattedTotalAmount = totalAmountTicket.toLocaleString('id-ID')
			$(".list-group").append(`<li class="list-group-item">Total amount : Rp. ${formattedTotalAmount}</li>`)
		}

		totalAmount = totalAmountTicket
	})

	$(".btn-submit").click(function() {
		const date = $("[name='date']").val();
		const token = $("meta[name='csrf-token']").attr("content");
		const jsonTicketSelected = JSON.stringify(ticketSelected)

		$.ajax({
			url: `{{ route('booking.store') }}`,
			type: "POST",
			cache: false,
			data: {
				"date": date,
				"ticket_selected": ticketSelected,
				"total_amount": totalAmount ?? 0,
				"_token": token
			},
			success:function(response) {
				// alert(response.message)
				window.location.assign('/application/booking?success_message=true')
			},
			error:function(error) {
				alert(error.responseJSON.message)
			}
		})
	})
</script>
@endsection