@extends('app.backend.layout.app')

@section('contents')
<style>
    .sidenav {
        display: none !important;
    }
</style>

<main class="main-content position-relative border-radius-lg ">
   <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<i class="fas fa-plus"></i>
						Booking Detail - {{ $booking->booking_code }}
					</div>
					<div class="card-body">
						@include('app.backend.auth.layout.partials.alert-message')
						<div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">User Name :</span>
									<input class="form-control" value="{{ $booking->user->name }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">Booking Code :</span>
									<input class="form-control" value="{{ $booking->booking_code }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">Date :</span>
									<input class="form-control" value="{{ date_format(new DateTime($booking->date), "d M Y") }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">Status :</span>
									<input class="form-control" value="{{ $booking->status }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<ul class="list-group">
									<li class="list-group-item active">List Ticket Selected</li>
									@foreach ($booking->bookingDetails as $item)
										<li class="list-group-item">
											Ticket: {{ $item->ticket->name }} Qty: {{ $item->qty }} Amount: Rp.{{ number_format($item->amount) }}
										</li>
									@endforeach
									<li class="list-group-item">Total Amount: Rp.{{ number_format($booking->amount) }}</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@endsection