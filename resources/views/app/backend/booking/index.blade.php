@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              @if(session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
              @if(auth()->user()->user_level == 'User')
                <a href="{{ route('booking.create') }}" class="btn btn-primary">Create Booking</a>
              @endif
              <h6>Bookings table</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center justify-content-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Booking Code</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">User Name</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Date</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Status</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($bookings as $booking)
                      <tr>
                        <td>
                          <h6 class="mb-0 text-sm" style="margin-left: 10px;">{{ $booking->booking_code }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $booking->user->name }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ date_format(new DateTime($booking->date), "d M Y") }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $booking->status }}</h6>
                        </td>
                        <td class="align-middle d-flex flex-wrap">
                          <a style="padding:4px 0;" class="btn btn-link text-secondary mb-0" href="{{ route('booking.show', $booking->id) }}">
                            <i class="fas fa-eye"></i>
                          </a>
                          <form method="post" action="{{ route('booking.delete', $booking->id) }}">
                            @csrf
                            @method('DELETE')
                            <button style="padding: 0;" type="submit" class="btn btn-link text-secondary mb-0 mx-2">
                              <i class="fas fa-trash"></i>
                            </button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
@endsection