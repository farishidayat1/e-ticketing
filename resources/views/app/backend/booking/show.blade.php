@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
   <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<i class="fas fa-plus"></i>
						Booking Detail - {{ $booking->booking_code }}
					</div>
					<div class="card-body">
						@include('app.backend.auth.layout.partials.alert-message')
						<div>
							@if($booking->barcode && auth()->user()->user_level == 'User')
								<div class="form-group">
									<label>QR Code</label>
									<div class="row">
										<img src="{{ $generateQr }}" style="width: 250px; height: auto;"/>
									</div>
								</div>

								<div class="form-group">
									<div class="input-group">
										<span class="input-group-text">Visitors to :</span>
										<input class="form-control" value="1" disabled style="background-color: #fff !important;">
									</div>
								</div>
							@endif
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">User Name :</span>
									<input class="form-control" value="{{ $booking->user->name }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">Booking Code :</span>
									<input class="form-control" value="{{ $booking->booking_code }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">Date :</span>
									<input class="form-control" value="{{ date_format(new DateTime($booking->date), "d M Y") }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<div class="input-group">
									<span class="input-group-text">Status :</span>
									<input class="form-control" value="{{ $booking->status }}" disabled style="background-color: #fff !important;">
								</div>
							</div>
							<div class="form-group">
								<ul class="list-group">
									<li class="list-group-item active">List Ticket Selected</li>
									@foreach ($booking->bookingDetails as $item)
										<li class="list-group-item">
											Ticket: {{ $item->ticket->name }} Qty: {{ $item->qty }} Amount: Rp.{{ number_format($item->amount) }}
										</li>
									@endforeach
									<li class="list-group-item">Total Amount: Rp.{{ number_format($booking->amount) }}</li>
								</ul>
							</div>
							@if(!$booking->proof_of_payment)
								<form method="POST" action="{{ route('booking.upload-proof-of-payment')}}" enctype="multipart/form-data">
									@csrf
									<input type="hidden" name="booking_id" value="{{ $booking->id }}">
									<div class="form-group">
										<label>Upload Proof Of Payment</label>
										<input type="file" class="form-control" name="proof_of_payment">
									</div>
									<button type="submit" class="btn btn-primary btn-submit">Pay for orders</button>
								</form>
							@else
								@if(auth()->user()->user_level != 'User')
								<div class="form-group">
									<label>Proof Of Payment</label>
									<div class="row">
										<img src="{{ asset('myfiles/'.$booking->proof_of_payment) }}" style="height: auto; width: 400px;">
									</div>
								</div>
								@endif	
							@endif

							@if(auth()->user()->user_level == "Admin" && $booking->status == 'Waiting Approval')
								<form method="POST" action="{{ route('booking.approve-payment')}}" enctype="multipart/form-data">
									@csrf
									<input type="hidden" name="booking_id" value="{{ $booking->id }}">
									<input type="hidden" name="approved" value="1">
									<button type="submit" class="btn btn-success btn-submit">Approve</button>
								</form>
							@endif
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@endsection