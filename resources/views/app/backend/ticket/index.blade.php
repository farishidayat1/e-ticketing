@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
    <div class="container-fluid py-4">
      <div class="row">
        <div class="col-12">
          <div class="card mb-4">
            <div class="card-header pb-0">
              @if(session('success'))
                  <div class="alert alert-success">
                      {{ session('success') }}
                  </div>
              @endif
              <a href="{{ route('ticket.create') }}" class="btn btn-primary">Create Ticket</a>
              <h6>Tickets table</h6>
            </div>
            <div class="card-body px-0 pt-0 pb-2">
              <div class="table-responsive p-0">
                <table class="table align-items-center justify-content-center mb-0">
                  <thead>
                    <tr>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7">Name</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Qty</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Price</th>
                      <th class="text-uppercase text-secondary text-xxs font-weight-bolder opacity-7 ps-2">Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @foreach($tickets as $ticket)
                      <tr>
                        <td>
                          <h6 class="mb-0 text-sm" style="margin-left: 10px;">{{ $ticket->name }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">{{ $ticket->qty }}</h6>
                        </td>
                        <td>
                          <h6 class="mb-0 text-sm">Rp.{{ number_format($ticket->price, 0) }}</h6>
                        </td>
                        <td class="align-middle d-flex flex-wrap">
                          <a style="padding:4px 0;" class="btn btn-link text-secondary mb-0" href="{{ route('ticket.show', $ticket->id) }}">
                            <i class="fas fa-eye"></i>
                          </a>
                          <form  method="post" action="{{ route('ticket.delete', $ticket->id) }}">
                            @csrf
                            @method('DELETE')
                            <button style="padding: 0;" type="submit" class="btn btn-link text-secondary mb-0 mx-2">
                              <i class="fas fa-trash"></i>
                            </button>
                          </form>
                        </td>
                      </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </main>
@endsection