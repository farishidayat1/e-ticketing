@extends('app.backend.layout.app')

@section('contents')
<main class="main-content position-relative border-radius-lg ">
    @include('app.backend.layout.partials.navbar')
   <div class="container">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
					<div class="card-header">
						<i class="fas fa-plus"></i>
						Edit {{  $ticket->name }}
					</div>
					<div class="card-body">
						@include('app.backend.auth.layout.partials.alert-message')
						<form method="POST" action="{{ route('ticket.update', $ticket->id) }}">
							@csrf
							<div class="form-group">
								<label>Name</label>
								<input type="name" name="name" class="form-control @error('name') is-invalid @enderror" value="{{  $ticket->name }}">
								@error('name')
								 	<div id="validationServerUsernameFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Qty</label>
								<input type="number" name="qty" class="form-control @error('qty') is-invalid @enderror" value="{{  $ticket->qty }}">
								@error('qty')
								 	<div id="validationServerqtyFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<div class="form-group">
								<label>Price</label>
								<input type="number" name="price" class="form-control @error('price') is-invalid @enderror" value="{{  $ticket->price }}">
								@error('price')
								 	<div id="validationServerpriceFeedback" class="invalid-feedback">
								        {{$message}}
								    </div>
								@enderror
							</div>
							<button class="btn btn-primary" type="submit">Update Ticket</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

@endsection