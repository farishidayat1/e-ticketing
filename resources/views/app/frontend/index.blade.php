<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Taman Bunga Wiladatika Cibubur</title>
    <link rel="stylesheet" href="{{ asset('assets/frontend/css/style.css') }}">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/css/bootstrap.min.css" integrity="sha384-Zenh87qX5JnK2Jl0vWa8Ck2rdkQ2Bzep5IDxbcnCeuOxjzrPF/et3URy9Bv1WTRi" crossorigin="anonymous">
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link rel="shorcut icon" href="{{ asset('assets/frontend/img/logo.png') }}">
    <link href="{{ asset('assets/frontend/owl/assets/owl.carousel.min.css') }}">
    <link href="{{ asset('assets/frontend/owl/assets/owl.theme.default.min.css') }}">

</head>
<body>
    <div class="section-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-expand-lg">
                        <div class="container-fluid">
                          <a class="navbar-brand" href="{{ url('/') }}">
                            <img class="logo-nav" src="{{ asset('assets/frontend/img/logo.png') }}"/>
                          </a>
                          {{-- <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                          </button> --}}
                          {{-- <div class="collapse navbar-collapse justify-content-end" id="navbarNav">
                            <ul class="navbar-nav">
                              <li class="nav-item">
                                <a class="nav-link" href="#">Home</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#">About us</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" href="#">Our Service</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link buttonLogin" href="#" target="_blank">Login</a>
                              </li>
                            </ul>
                          </div> --}}
                        </div>
                    </nav>
                </div>    
            </div>            
        </div>
    </div>


    <div id="carouselExampleIndicators" class="carousel slide">
      <div class="carousel-indicators">
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="1" aria-label="Slide 2"></button>
        <button type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide-to="2" aria-label="Slide 3"></button>
      </div>
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="section-background" style="background-image: url('{{ asset('assets/frontend/img/1.jpeg') }}');">
          <div class="table">
              <div class="table-cell">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-7">
                              <div class="title">
                                  <h1>Taman Bunga Wiladatika Cibubur</h1>
                                  <p>
                                    Taman Wisata Wiladatika adalah sebuah tempat wisata yang berlokasi di Cimanggis, Depok. Disini terdapat Pusat Pendidikan dan Pelatihan Pramuka Nasional (Pusdiklatmas), Balai Pembinaan Pendidikan Pelaksanaan Pedoman Penghayatan dan Pengamalan Pancasila (P4), aula resepsi yang biasa digunakan untuk acara resepsi pernikahan dan halaman hijau yang biasa digunakan para pengunjung untuk piknik bersama keluarga.
                                  </p>
                                  <div class="d-block mt-3">
                                      <a class="btn buttonBuy" href="{{ route('login') }}">
                                          Beli tiket sekarang !
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
        </div>
        <div class="carousel-item">
           <div class="section-background" style="background-image: url('{{ asset('assets/frontend/img/2.jpeg') }}');">
          <div class="table">
              <div class="table-cell">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-7">
                              <div class="title">
                                  <h1>Taman Bunga Wiladatika Cibubur</h1>
                                  <p>
                                    Taman Wisata Wiladatika adalah sebuah tempat wisata yang berlokasi di Cimanggis, Depok. Disini terdapat Pusat Pendidikan dan Pelatihan Pramuka Nasional (Pusdiklatmas), Balai Pembinaan Pendidikan Pelaksanaan Pedoman Penghayatan dan Pengamalan Pancasila (P4), aula resepsi yang biasa digunakan untuk acara resepsi pernikahan dan halaman hijau yang biasa digunakan para pengunjung untuk piknik bersama keluarga.
                                  </p>
                                  <div class="d-block mt-3">
                                      <a class="btn buttonBuy" href="{{ route('login') }}">
                                          Beli tiket sekarang !
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
        </div>
        <div class="carousel-item">
           <div class="section-background" style="background-image: url('{{ asset('assets/frontend/img/3.jpeg') }}');">
          <div class="table">
              <div class="table-cell">
                  <div class="container">
                      <div class="row">
                          <div class="col-md-7">
                              <div class="title">
                                  <h1>Taman Bunga Wiladatika Cibubur</h1>
                                  <p>
                                    Taman Wisata Wiladatika adalah sebuah tempat wisata yang berlokasi di Cimanggis, Depok. Disini terdapat Pusat Pendidikan dan Pelatihan Pramuka Nasional (Pusdiklatmas), Balai Pembinaan Pendidikan Pelaksanaan Pedoman Penghayatan dan Pengamalan Pancasila (P4), aula resepsi yang biasa digunakan untuk acara resepsi pernikahan dan halaman hijau yang biasa digunakan para pengunjung untuk piknik bersama keluarga.
                                  </p>
                                  <div class="d-block mt-3">
                                      <a class="btn buttonBuy" href="{{ route('login') }}">
                                          Beli tiket sekarang !
                                      </a>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
        </div>
      </div>
      <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Previous</span>
      </button>
      <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleIndicators" data-bs-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="visually-hidden">Next</span>
      </button>
    </div>
    </div>
    <div class="section-info " style="overflow:hidden">
      <div class="container">
        <div class="row d-flex justify-content-center align-items-center">
          <div class="col-md-6">
            <div class="description" data-aos="zoom-in">
              <h1>Sejarah Taman Bunga Wiladatika Cibubur</h1>
              <p>
                Taman Rekreasi ini diresmikan oleh Presiden Soeharto pada tahun 1980 tepatnya pada tanggal 29 Juni. Dibukanya taman ini untuk umum, dikelola oleh Badan Pembinaan Pendidikan Pelaksanaan Pedoman Penghayatan dan Pengamalan Pancasila bersama Gerakan Pramuka Indonesia bertujuan rekreasi berbasis pengamalan Pancasila serta untuk mengumpulkan dana yang akan digunakan sebagai pendanaan kegiatan kepramukaan.
              </p>
            </div>
          </div>
          <div class="col-md-6">
            <img src="https://cdn-cas.orami.co.id/parenting/images/visaliakotawisata.id.width-1000.jpg"/>
          </div>
        </div>
      </div>
    </div>
    <div class="section-service">
      <div class="container">
        <div class="row">
          <div class="col-md-12">
            <div class="title text-center">
              <h1>Apa saja keuntungan pembelian melalui website?</h1>
            </div>
            <div class="d-flex flex-wrap flex-row justify-content-around mt-5">
              <div class="benefits" data-aos="zoom-in" data-aos-duration="500"> 
                <img src="https://cdn-icons-png.flaticon.com/512/3798/3798326.png" width="150" alt="">
                <h5 class="text-center mt-3">Lebih Mudah</h5>
              </div>
              <div class="benefits" data-aos="zoom-in" data-aos-duration="1000"> 
                <img src="https://cdn-icons-png.flaticon.com/512/1584/1584892.png" width="150" alt="">
                <h5 class="text-center mt-3">Proses Cepat</h5>
              </div>
              <div class="benefits" data-aos="zoom-in" data-aos-duration="1500"> 
                <img src="https://cdn-icons-png.flaticon.com/512/1067/1067566.png" width="150" alt="">
                <h5 class="text-center mt-3">Pelayanan 24 Jam</h5>
              </div>
              <div class="benefits" data-aos="zoom-in" data-aos-duration="2000"> 
                <img src="https://cdn-icons-png.flaticon.com/512/4048/4048067.png" width="150" alt="">
                <h5 class="text-center mt-3">Luasnya Informasi</h5>
              </div>
              <div class="benefits" data-aos="zoom-in" data-aos-duration="2500"> 
                <img src="https://cdn-icons-png.flaticon.com/512/5973/5973481.png" width="150" alt="">
                <h5 class="text-center mt-3">Approved dengan Cepat</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    
    <div class="section-ticket">
      <div class="container" style="overflow: hidden">
        <div class="row">
          <div class="col-md-12">
            <div class="title text-center">
              <h2>Daftar harga tiket</h2>
            </div>
          </div>
        
        </div>    
        <div class="row">
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/4.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Kolam Renang</h5>
                <p class="card-text">Paket Kolam Renang beserta tiket Masuk</p>
                <a href="#">RP. 33000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/5.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Bola Air</h5>
                <p class="card-text">Paket Bola Air beserta tiket Masuk</p>
                <a href="#">RP. 20000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/6.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Becak Mini</h5>
                <p class="card-text">Paket Becak Mini beserta tiket Masuk</p>
                <a href="#">RP. 20000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/2.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Perahu air</h5>
                <p class="card-text">Paket Perahu Air beserta tiket Masuk</p>
                <a href="#">RP. 20000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/3.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Sepeda</h5>
                <p class="card-text">Paket Sepeda beserta tiket Masuk</p>
                <a href="#">RP. 13000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/9.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Poto Prewedding</h5>
                <p class="card-text">Paket Prewedding </p>
                <a href="#">RP. 275000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/3.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Poto Amatir</h5>
                <p class="card-text">Paket Amatir </p>
                <a href="#">RP. 100000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/7.png') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Shooting Film</h5>
                <p class="card-text">Paket Shooting Film </p>
                <a href="#">RP. 27000000</a>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="card" style="width: 22rem; margin:20px 0;">
              <img src="{{ asset('assets/frontend/img/8.jpeg') }}" class="card-img-top" alt="...">
              <div class="card-body">
                <h5 class="card-title">Poto Iklan</h5>
                <p class="card-text">Paket poto iklan </p>
                <a href="#">RP. 550000</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.1842682612623!2d106.8911383141617!3d-6.370194664085534!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69eca341d5feeb%3A0x1475ebc18101ced!2sTaman%20Rekreasi%20Wiladatika!5e0!3m2!1sid!2sid!4v1678085472117!5m2!1sid!2sid" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>

    <div class="section-background" style="background-image: url('{{ asset('assets/frontend/img/7.png') }}'); height:300px">
      <div class="table">
          <div class="table-cell">
              <div class="container">
                  <div class="row">
                      <div class="col-md-12">
                          <div class="title text-center">
                              <h1>Kami tunggu kedatangan kalian!</h1>
                              <div class="d-block mt-3">
                                  <a class="btn buttonBuy m-auto" style="width: 400px" href="{{ route('login') }}">
                                    Daftarkan segera dan beli tiketnya
                                  </a>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>
  
  <script src="https://code.jquery.com/jquery-3.6.3.js" integrity="sha256-nQLuAZGRRcILA+6dMBOvcRh5Pe310sBpanc6+QBmyVM=" crossorigin="anonymous"></script>    
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.2/dist/js/bootstrap.min.js" integrity="sha384-IDwe1+LCz02ROU9k972gdyvl+AESN10+x7tBKgc9I5HFtuNz0wWnPclzo6p9vxnk" crossorigin="anonymous"></script>
  <script src="{{ asset('assets/frontend/js/typewriter.js') }}"></script>
  <script src="{{ asset('assets/frontend/owl/owl.carousel.min.js') }}"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <script>
        $(document).ready(function(){
          $(".owl-carousel").owlCarousel();
        });
    </script>
    <script>
      AOS.init();
    
    </script>
</body>
</html>